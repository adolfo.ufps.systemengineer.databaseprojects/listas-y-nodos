/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Modelo.Estudiante;
import Modelo.SecuenciaEstudiante;

/**
 *
 * @author profesor
 */
public class TestSecuenciaEstudiante {

    public static void main(String[] args) {
        Estudiante x1 = new Estudiante("Santiago", 1500, 3.5F, 3.5F, 3.5F, 5F);
        Estudiante x2 = new Estudiante("Samanta", 1501, 4.5F, 4.5F, 3.5F, 5F);
        Estudiante x3 = new Estudiante("Ronald", 1502, 2.5F, 3.5F, 3.5F, 5F);

        SecuenciaEstudiante lista = new SecuenciaEstudiante(new Estudiante[] { x1, x2, x3 });
        System.out.println("Lista desordenada:");
        System.out.println(lista);

        System.out.println("Lista Ordenada:");
        lista.sort();
        System.out.println(lista);
    }
}
