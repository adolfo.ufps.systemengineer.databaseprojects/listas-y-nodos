package Vista;

import Util.Secuencia;

public class TestSecuencia {
    public static void main(String[] args) {

        Integer[] x = new Integer[10];

        @SuppressWarnings({ "rawtypes", "unchecked" })
        Secuencia<Integer> secuencia = new Secuencia(x);

        for (int i = 0; i < secuencia.size(); i++) {
            if (i % 2 == 0) {
                secuencia.add(i, 2);
            } else {
                secuencia.add(i, secuencia.size() - i);
            }
        }

        System.out.println("\n" + secuencia + "\n");

        secuencia.sort();

        System.out.println("\n" + secuencia + "\n");

        System.out.println("\nEl elemento mayor es: " + secuencia.getMayor());
        System.out.println("\nEl elemento menor es: " + secuencia.getMenor());
    }
}
