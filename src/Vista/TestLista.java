/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.ListaS;

/**
 *
 * @author profesor
 */
public class TestLista {

    public static void main(String[] args) {
        @SuppressWarnings({ "rawtypes", "unchecked" })
        ListaS<Integer> lista1 = new ListaS();
        for (int i = 0; i < 10; i++) {
            lista1.insertarInicio(i + 1);
        }

        System.out.println(lista1);
        try {
            if (lista1.contains(4)) {
                System.out.println("existe");
            } else {
                System.out.println("no existe");
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        System.out.println("Implementando metodo virus");
        lista1.virus(4, 4);
        System.out.println(lista1);
    }
}
