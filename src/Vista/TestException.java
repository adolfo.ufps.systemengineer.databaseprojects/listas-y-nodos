/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Modelo.EnteroGrande;
import java.util.Scanner;

/**
 *
 * @author Docente
 */
public class TestException {

    /**
     * 
     * @param args 
     */
    public static void main(String[] args) {

        EnteroGrande n = crear();
        System.out.println(n);
        
        EnteroGrande n2 = crearConLimites();
        System.out.println(n2);
        
    }

    @SuppressWarnings("resource")
    private static EnteroGrande crear() {
        EnteroGrande x = null;
        try {
            System.out.println("Digite tamaño vector:");
            x = new EnteroGrande(new Scanner(System.in).nextInt());
        } catch (Exception ex) {
            System.out.println("Error, vuelva y digite");
            crear();
        }
        return x;
    }
    
    
    @SuppressWarnings("resource")
    private static EnteroGrande crearConLimites() {
        EnteroGrande x = null;
        try {
            System.out.println("Digite limite inicial y final:");
            x = new EnteroGrande(new Scanner(System.in).nextInt(), new Scanner(System.in).nextInt());
        } catch (Exception ex) {
            System.out.println("Error, vuelva y digite");
            crear();
        }
        return x;
    }
    
    
}
