/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Util;

/**
 * Esta clase modela un nodo, que es un contenedor que contiene un objeto
 * genérico y un nodo siguiente, una clase de apuntador (---->)
 * 
 * @author profesor
 */
class Nodo<T> {

    /**
     * El elemento genérico que contiene
     */
    private T elemento;

    /**
     * El nodo al que apunta este nodo
     */
    private Nodo<T> sig;

    Nodo() {
    }

    /**
     * Crea un nodo que contendrá el apuntador y la información del objeto genérico
     * contenido
     * 
     * @param elemento El objeto<T> contenido
     * @param sig      El nodo siguiente
     */
    Nodo(T elemento, Nodo<T> sig) {
        this.elemento = elemento;
        this.sig = sig;
    }

    /**
     * Retorna el objeto contendio dentro del nodo, recordando que el nodo solo es
     * un contenedor, no el objeto<T> final
     * 
     * @return Retorna el objeto
     */
    T getInfo() {
        return elemento;
    }

    /**
     * Obtiene el nodo siguiente de este
     * 
     * @return Retorna el nodo siguiente
     */
    Nodo<T> getSig() {
        return sig;
    }

    /**
     * Setea el elemento contendio dentro de este nodo
     * 
     * @param elemento Recibe el elemento que ocupará este nodo
     */
    void setElement(T elemento) {
        this.elemento = elemento;
    }

    /**
     * Setea el nodo que le sigue a este nodo
     * @param sig Recibe el nodo por el cuál se va a setear
     */
    void setSig(Nodo<T> sig) {
        this.sig = sig;
    }
}
