/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Util;

/**
 * Esta clase contiene un nodo, y como un nodo puede apuntar, entonces al tener
 * un nodo, se tendrán también los nodos siguientes. Este nodo que contiene la
 * lista es la cabeza,
 * 
 * @author profesor
 */
public class ListaS<T> {

    /**
     * Esta es la cabeza del nodo. Generalmente es el primer elemento que conforme
     * la lista, y apunta directamente al elemento siguiente, pues es un nodo
     */
    private Nodo<T> cabeza;

    /**
     * El tamaño de la lista. El número de nodos que tiene
     */
    private int size;

    public ListaS() {
        this.cabeza = null;
    }

    /**
     * Esto inserta un nuevo elemento en el espacio inicial que corresponde a la
     * cabeza del nodo.
     * 
     * @param objeto Recibe un elemento genérico cualquiera
     */
    public void insertarInicio(T objeto) {
        @SuppressWarnings({ "rawtypes", "unchecked" })
        Nodo<T> nuevo = new Nodo(objeto, this.cabeza);
        this.cabeza = nuevo;
        size++;
    }

    /**
     * Retorna un valor para comprobar que la lista es vacía
     * 
     * @return 'True' si es vacía y 'False' si no
     */
    public boolean esVacio() {
        return this.cabeza == null;
    }

    /**
     * Tiene como objetivo aportar el tamaño de la lista
     * 
     * @return Retorna un entero que da el tamaño de la lista
     */
    public int getSize() {
        return size;
    }

    /**
     * Lo que hace es crear una Cadena que tendrá forma de lista, donde todos los
     * elementos apuntan directamente a su elemento siguiente
     */
    public String toString() {
        String msg = "cab<>";
        for (Nodo<T> i = this.cabeza; i != null; i = i.getSig()) {
            msg += i.getInfo().toString() + "->";
        }
        return msg + "null";
    }

    /**
     * Valida si la lista contiene un objeto determinado. Para esto, recorre con un
     * for, donde en lugar de sumarle a un índice 'i', 'i' será el siguiente de cada
     * nodo por cada iteración.
     * 
     * @param objeto Recibe el objeto a comparar
     * @return Retorna 'True' si lo contiene, y 'False' si no.
     */
    public boolean contains(T objeto) {
        if (objeto == null || this.esVacio()) {
            throw new RuntimeException(
                    "No puede buscar un elemento porque el objeto es nulo o porque la lista está vacía...");
        }
        for (Nodo<T> i = this.cabeza; i != null; i = i.getSig()) {
            if (i.getInfo().equals(objeto)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Vuelve la cabeza nula, lo que hace imposible acceder al resto de elementos,
     * lo que es casi como borrarlos
     */
    public void clear() {
        this.cabeza = null;
        this.size = 0;
    }

    /**
     * Debe regresar el último elemento de la lista
     * 
     * @return Retorna el último elemento o nulo si la lista no tiene nada.
     */
    private Nodo<T> getUltimo() {
        for (Nodo<T> i = cabeza; i != null; i = i.getSig()) {
            if (i.getSig() == null) {
                return i;
            }
        }
        return null; // siempre va a retonar algo
    }

    /**
     * Recorre los nodos de la lista para ir por el último
     * 
     * @param objeto Retorna el último elemento de la lista
     */
    public void insertarFinal(T objeto) {
        @SuppressWarnings({ "rawtypes", "unchecked" })
        Nodo<T> nuevo = new Nodo(objeto, null);
        this.getUltimo().setSig(nuevo);
        size++;
    }

    /**
     * Solo mete al final de la lista un mismo elemento 'n' veces
     * @param info El elemento que se va a añadir
     * @param n La cantidad de veces que será añadido el elemento
     */
    public void virus(T info, int n) {
        for (int i = 0; i < n; i++) {
            insertarFinal(info);
        }
    }

}
