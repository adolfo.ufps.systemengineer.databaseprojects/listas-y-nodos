/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Util;

import java.util.Arrays;

/**
 * Estructura de datos estática
 *
 * @author Docente
 * @param <T>
 */
@SuppressWarnings("rawtypes")
public class SecuenciaS<T extends Comparable> {

    private T[] vector;

    public SecuenciaS() {
    }

    public SecuenciaS(T[] vector) {
        this.vector = vector;
    }

    // Crear:
    @SuppressWarnings("unchecked")
    public SecuenciaS(int n) {
        if (n <= 0) {
            throw new RuntimeException("Error dato debe ser >0:" + n);
        }
        // Ideal:s
        // this.vector=new T[n];
        Object temp[] = new Object[n];
        this.vector = (T[]) temp;
    }

    // Insertar:
    public void add(int i, T elemento) {
        this.validar(i);
        this.vector[i] = elemento;
    }

    public void set(int i, T elemento) {
        this.add(i, elemento);
    }

    public T get(int i) {
        this.validar(i);
        return this.vector[i];
    }

    public int size() {
        return this.vector.length;
    }

    private void validar(int i) {
        if (i < 0 || i >= this.vector.length) {
            throw new RuntimeException("Error dato fuera de rango:" + i);
        }
    }

    public void sort() {

        T ArrayN[] = this.vector;
        /* Bucle desde 0 hasta la longitud del array -1 */
        for (int i = 0; i < ArrayN.length - 1; i++) {
            /* Bucle anidado desde 0 hasta la longitud del array -1 */
            for (int j = 0; j < ArrayN.length - 1; j++) {
                /*
                 * Si el número almacenado en la posición j es mayor que el de la posición j+1
                 * (el siguiente del array)
                 */
                @SuppressWarnings("unchecked")
                int c = ArrayN[j].compareTo(ArrayN[j + 1]);
                if (c > 0) {
                    /* guardamos el número de la posicion j+1 en una variable (el menor) */
                    T temp = ArrayN[j + 1];
                    /* Lo intercambiamos de posición */
                    ArrayN[j + 1] = ArrayN[j];
                    ArrayN[j] = temp;
                    /*
                     * y volvemos al inicio para comparar los siguientes hasta que todos se hayan
                     * comparado de esta forma vamos dejando los números mayores al final del array
                     * en orden
                     */
                }
            }
        }

    }

    /**
     * Obtiene el elemento menor del arreglo
     * 
     * @return Retorna el elemento menor del arreglo
     */
    public T getMenor() {
        if (this.getVector() == null) {
            System.out.println("Imposible obtener el menor de un vector vacío...");
        }
        this.sort();
        return this.get(0);
    }

    /**
     * Obtiene el elemento mayor del arreglo
     * 
     * @return Retorna el elemento mayor del arreglo
     */
    public T getMayor() {
        if (this.getVector() == null) {
            System.out.println("Imposible obtener el mayor de un vector vacío...");
        }
        this.sort();
        return this.get(this.size() - 1);
    }

    /**
     * Este método genera la unión entre dos Secuencias, de forma que sus elementos
     * formen una nueva secuencia.
     * 
     * @param vector2 Recibe una nueva secuencia para evaluarla localmente
     * @return Retorna una nueva secuencia con los elementos de ambas secuencias
     */
    public SecuenciaS<T> getUnion(SecuenciaS<T> vector2) {
        SecuenciaS<T> secuenciaLocal = this.omitirRepetidos();
        SecuenciaS<T> secuenciaExterna = vector2.omitirRepetidos();

        SecuenciaS<T> secuenciaUnion = new SecuenciaS<T>(
                secuenciaLocal.size() + secuenciaExterna.size() - secuenciaLocal.elementosComunes(secuenciaExterna));
        int x = 0;
        for (int i = 0; i < secuenciaLocal.size(); i++) {
            if (secuenciaLocal.get(i) != null) {
                secuenciaUnion.add(x, secuenciaLocal.get(i));
                x++;
            }
        }
        for (int i = 0; i < secuenciaExterna.size(); i++) {
            if (!existe(secuenciaExterna.get(i), secuenciaLocal.getVector())) {
                secuenciaUnion.add(x, secuenciaExterna.get(i));
                x++;
            }

        }

        return secuenciaUnion;
    }

    /**
     * Éste método modifica la secuencia y retorna una nueva que contendrá solo
     * elementos no repetidos de la original
     * 
     * @return Retorna una nueva secuencia con elementos únicos
     */
    public SecuenciaS<T> omitirRepetidos() {
        SecuenciaS<T> secuenciaTemp = new SecuenciaS<>(this.vector.length);
        int x = 0;
        for (int i = 0; i < this.vector.length; i++) {
            if (i == 0) {
                secuenciaTemp.add(i, this.vector[x]);
                x++;
            }
            if (!existe(this.vector[i], secuenciaTemp.getVector())) {
                secuenciaTemp.add(x, this.vector[i]);
                x++;
            }

        }

        SecuenciaS<T> secuenciaFinal = new SecuenciaS<>(secuenciaTemp.size() - secuenciaTemp.elementosVacios());
        for (int i = 0; i < secuenciaFinal.size(); i++) {
            if (secuenciaTemp.get(i) != null) {
                secuenciaFinal.add(i, secuenciaTemp.get(i));
            }
        }

        return secuenciaFinal;
    }

    /**
     * Este método cuenta los elementos nulos o vacíos dentro de la secuencia (El
     * Array contenido)
     * 
     * @return Retorna un int cantidad de elementos vacíos
     */
    public int elementosVacios() {
        int count = 0;
        for (int i = 0; i < this.vector.length; i++) {
            if (this.vector[i] == null) {
                count++;
            }
        }
        return count;
    }

    public SecuenciaS<T> getInterseccion(SecuenciaS<T> vector2) {
        SecuenciaS<T> secuenciaLocal = this.omitirRepetidos();
        SecuenciaS<T> secuencia2 = vector2.omitirRepetidos();
        if (secuenciaLocal.elementosComunes(vector2) == 0)
            throw new RuntimeException("No hay elementos comunes");
        SecuenciaS<T> secuenciaInterseccion = new SecuenciaS<T>(secuenciaLocal.elementosComunes(secuencia2));
        int x = 0;
        for (int i = 0; i < secuenciaLocal.size(); i++) {
            for (int j = 0; j < secuencia2.size(); j++) {
                if (secuenciaLocal.get(i).equals(secuencia2.get(j))) {
                    secuenciaInterseccion.add(x, secuenciaLocal.get(i));
                    x++;
                }
            }
        }
        return secuenciaInterseccion;
    }

    public SecuenciaS<T> getDiferencia(SecuenciaS<T> vector2) {
        SecuenciaS<T> secuenciaLocal = this.omitirRepetidos();
        SecuenciaS<T> secuencia2 = vector2.omitirRepetidos();
        if (secuenciaLocal.elementosComunes(secuencia2) == secuenciaLocal.size())
            throw new RuntimeException("No hay elementos comunes");
        SecuenciaS<T> secuenciaDiferencia = new SecuenciaS<T>(
                secuenciaLocal.size() - secuenciaLocal.elementosComunes(secuencia2));
        int x = 0;
        for (int i = 0; i < secuenciaLocal.size(); i++) {
            if (!existe(secuenciaLocal.get(i), secuencia2.getVector())) {
                secuenciaDiferencia.add(x, secuenciaLocal.get(i));
                x++;
            }
        }

        return secuenciaDiferencia;
    }

    public SecuenciaS<T> getDiferenciaSimetrica(SecuenciaS<T> vector2) {
        SecuenciaS<T> a_b = new SecuenciaS<T>(this.getDiferencia(vector2).getVector());
        SecuenciaS<T> b_a = new SecuenciaS<T>(vector2.getDiferencia(this).getVector());
        if (a_b.size() == 0 && b_a.size() == 0)
            throw new RuntimeException("No hay diferecia simetrica");
        SecuenciaS<T> secuenciaDiferenciaSimetrica = new SecuenciaS<T>(a_b.size() + b_a.size());
        int x = 0;
        for (int i = 0; i < a_b.size(); i++) {
            secuenciaDiferenciaSimetrica.add(x, a_b.get(i));
            x++;
        }
        for (int i = 0; i < b_a.size(); i++) {
            secuenciaDiferenciaSimetrica.add(x, b_a.get(i));
            x++;
        }
        return secuenciaDiferenciaSimetrica;
    }

    /**
     * Crea una nueva secuencia cuyo vector, con ayuda del método
     * secuenciasVector(int), almacenará más secuencias, que serán los conjuntos
     * pares.
     * 
     * @return Retorna una nueva secuencia cuyo vector estarpa compuesto de más
     *         secuencias.
     */

    public SecuenciaS<T> getConjuntosPares() {
        SecuenciaS<T> xpress = new SecuenciaS<T>();
        xpress.setVector(secuenciasVector());
        return xpress;
    }

    /**
     * Este método busca crear un nuevo vector T[] en el cuál se almacenarán en cada
     * posición T[i] una secuencia<T>. Por ahora, solo crea un array del tamaño
     * .length de This.Vector, y en cada espacio secuencia[i] mete un elemento de
     * this.vector
     * 
     * @return Retorna un vector T[] que se podrá usar para setear un vector de otra
     *         secuencia<T>, y de esta forma, crear una secuencia de secuencias
     */
    @SuppressWarnings("unchecked")
    public SecuenciaS<T>[] secuenciasVector() {

        SecuenciaS<T>[] arrayPotencia = new SecuenciaS[(int) (Math.pow(2, size()))]; // Crea el Array de Secuencias
        int x = 0;
        // Inicializa cada secuencia en el Array
        for (int i = 0; i < size(); i++) {
            SecuenciaS<T> arrayTemp = new SecuenciaS<>((T[]) new Comparable[] { this.vector[i] });
            for (int j = 0; j < arrayPotencia.length; j++) {
                if (arrayPotencia[j] == null)
                    continue;
                SecuenciaS<T> addTempo = arrayTemp.getUnion((arrayPotencia[j]));
                if (!addTempo.equals(arrayPotencia[j])) {
                    arrayPotencia[x] = addTempo;
                    x++;
                }
            }
            arrayPotencia[x] = arrayTemp;
            x++;
        }
        this.printPares(arrayPotencia);
        return arrayPotencia;
    }

    private void printPares(SecuenciaS<T>[] secuencias) {

        for (int i = 0; i < secuencias.length; i++) {
            System.out.print("{");
            if (secuencias[i] == null) {
                System.out.println("null}");
                continue;
            }
            for (int j = 0; j < secuencias[i].size(); j++) {
                if (secuencias[i].size() == 1) {
                    System.out.print(secuencias[i].get(j));
                } else if (j == secuencias[i].size() - 1) {
                    System.out.print(secuencias[i].get(j));
                } else
                    System.out.print(secuencias[i].get(j) + ",");

            }
            System.out.print("}\n");

        }

    }

    /**
     * Este método cuenta los elementos repetidos encontrados dentro de un Array en
     * una secuencia y devuelve esa cantidad
     * 
     * @param vector2 Recibe la secuencia con la cuál se va a comparar
     * @return Devuelve la cantidad de elementos comunes
     */
    public int elementosComunes(SecuenciaS<T> vector2) {
        int count = 0;
        for (int i = 0; i < this.vector.length; i++) {
            for (int j = 0; j < vector2.getVector().length; j++) {
                if (this.vector[i].equals(vector2.getVector()[j])) {
                    count++;
                }
            }
        }
        return count;
    }

    /**
     * Comprueba si un elemento evaluado de forma local, existe dentro de un Array
     * externo
     * 
     * @param elemento Recibe un elemento genérico para comparar
     * @param vector   Recibe un Array dentro del cuál se va a evaluar el elemento
     * @return True si existe, false si no
     */
    public boolean existe(T elemento, T[] vector) {
        for (int i = 0; i < vector.length; i++) {
            if (vector[i] == elemento)
                return true;
        }
        return false;
    }

    public T[] getVector() {
        return this.vector;
    }

    public void setVector(SecuenciaS<T>[] secuencias) {
        this.vector = convertToGenericArray(secuencias);
    }

    private T[] convertToGenericArray(SecuenciaS<T>[] secuencias) {
        @SuppressWarnings("unchecked")
        T[] genericArray = (T[]) new Comparable[secuencias.length];
        for (int i = 0; i < secuencias.length; i++) {
            if (secuencias[i] == null) {
                genericArray[i] = null;
                continue;
            }
            genericArray[i] = secuencias[i].getVector()[0];
        }
        return genericArray;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        @SuppressWarnings("unchecked")
        SecuenciaS<T> other = (SecuenciaS<T>) obj;
        if (!Arrays.equals(vector, other.vector))
            return false;
        return true;
    }

    @Override
    public String toString() {
        String txt = "";
        for (int i = 0; i < this.vector.length; i++) {
            if (this.vector[i] != null)
                txt += this.vector[i] + ", ";
            else
                txt += "null\n";
        }
        return txt;
    }

}
