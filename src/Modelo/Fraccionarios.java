/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import Util.Secuencia;

/**
 *
 * @author Docente
 */
public class Fraccionarios {
    
    private Secuencia<Fraccionario> fracciones;

    public Fraccionarios() {
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public Fraccionarios(Fraccionario[] est) {
        this.fracciones = new Secuencia(est);
    }

    @Override
    public String toString() {

        String msg = "";
        for (int i = 0; i < this.fracciones.size(); i++) {
            Fraccionario f = this.fracciones.get(i);
            msg += f.toString() + "\n";

        }
        return msg;
    }

    public void sort()
    {
        this.fracciones.sort();
    }
}
