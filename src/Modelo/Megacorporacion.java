package Modelo;

/**
 * Clase que simula un objeto que es una Megacorporacion multinacional
 * capitalista (づ￣ 3￣)づ
 * 
 * @author Adolfo Alejandro Arenas Ramos (1152370)
 * @author Johan Daniel García Salcedo (1152379)
 */
public class Megacorporacion implements Comparable<Megacorporacion> {

    /**
     * Se definen los parámetros que tendrá nuestro objeto "Megacorporación".
     */
    private String nombre, ceo, pais, ciudad;

    /**
     * Constructor vacío
     */
    public Megacorporacion() {
    }

    /**
     * Constructor con parámetros
     * 
     * @param dt3 El nombre de la Megacorporación
     * @param dt2 La direccion de la Megacorporación
     * @param dt4 El país de la Megacorporación
     * @param dt5 El CEO de la Megacorporación
     * @param dt6 La ciudad principal de la Megacorporación
     */
    public Megacorporacion(String nombre, String ceo, String pais, String ciudad) {
        this.nombre = nombre;
        this.ceo = ceo;
        this.pais = pais;
        this.ciudad = ciudad;
    }

    /**
     * Este método imprime todo lo relacionado a la megacorporación
     */
    @Override
    public String toString() {
        String rta = "\nMegacorporación:" + "\n\tNombre: " + this.nombre + "\n\tCEO: " + this.ceo + "\n\tPais: "
                + this.pais + "\n\tCiudad:" + this.ciudad + "\n";
        return rta;
    }

    /**
     * Compara si este objeto Megacorporacion y otro objeto externo Megacorporacion
     * son iguales. Este método compara si tres de los parámetros que conforman a una
     * megacorporacion son iguales a los de otra megacorporacion. Se elige el
     * nombre, el CEO, y la ciudad, bajo la lógica de que una companía puede
     * llamarse igual, estar en el mismo país, tener el mismo CEO, pero no compartir
     * la misma ciudad.
     * 
     * @param megacorporacion La megacorporacion con la cual se quiere comparar
     * @return False si son diferentes y True si son iguales
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Megacorporacion t = (Megacorporacion) obj;
        if ((this.getNombre() == t.getNombre()) && (this.getCeo() == t.getCeo())
                && (this.getCiudad() == t.getCiudad())) {
            return true;
        }
        return false;
    }

    /**
     * Este método compara a este objeto Megacorporación con otro objeto externo
     * Megacorporacion de forma lexicográfica en tres ciclos. Uno compara el nombre,
     * si son iguales, compara el nombre del CEO, y si son iguales, compara la
     * ciudad. (SE USA ORDEN ALFABÉTICO)
     * 
     * @return Si retorna un negativo, la otra corporacion es Mayor, caso contrario
     *         es menor
     */
    @Override
    public int compareTo(Megacorporacion otraCorporacion) {
        int comparacionNombre = this.nombre.compareTo(otraCorporacion.nombre);
        if (comparacionNombre != 0) {
            return comparacionNombre;
        }

        int comparacionCEO = this.ceo.compareTo(otraCorporacion.ceo);
        if (comparacionCEO != 0) {
            return comparacionCEO;
        }

        return this.ciudad.compareTo(otraCorporacion.ciudad);
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCeo() {
        return this.ceo;
    }

    public void setCeo(String ceo) {
        this.ceo = ceo;
    }

    public String getPais() {
        return this.pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getCiudad() {
        return this.ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }
}
