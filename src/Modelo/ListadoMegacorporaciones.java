package Modelo;

import Util.Secuencia;

public class ListadoMegacorporaciones {

    private Secuencia<Megacorporacion> megacorporaciones;

    public ListadoMegacorporaciones() {
    }

    public ListadoMegacorporaciones(Megacorporacion[] corporaciones) {
        this.megacorporaciones = new Secuencia<Megacorporacion>(corporaciones);
    }

    public ListadoMegacorporaciones(Secuencia<Megacorporacion> corporaciones) {
        this.megacorporaciones = corporaciones;
    }

    /**
     * Este método sobreescribe el toString() de la clase object.
     * 
     * @return Retorna todos los nombres de las megacorporaciones almacenadas por la
     *         secuencia this
     */
    @Override
    public String toString() {
        String txt = "";
        for (int i = 0; i < this.megacorporaciones.size(); i++) {
            Object x = this.megacorporaciones.get(i);
            System.out.println(x);
            if(x instanceof Secuencia){

                for(int j = 0; j < ((Secuencia)x).size(); j++)  
                txt += ((Secuencia)x).getVector()[i].toString()+ ",";
            }
            if (x != null)
                txt += x.toString() + "\n";
            else
                txt += "null\n";
            
        }
        return txt;
    }

    /**
     * Este método usará el sort de Secuencia() para organizar las megacorporaciones
     * en orden alfabético
     */
    public void sort() {
        this.megacorporaciones.sort();
    }

    /**
     * Este método simula una unión de contjuntos entre dos objetos
     * ListaMegacorporaciones distintos, uno implícito que será el que ejecute el
     * método, y otro externo. Los elementos repetidos no se recibirán
     * 
     * @param x Será el objeto ListaMegacorporaciones con el cuál se comparará el
     *          this
     * @return Retorna un nuevo objeto ListaMegacorporaciones que contendrá la unión
     *         entre ambos objetos
     */
    public ListadoMegacorporaciones getUnion(ListadoMegacorporaciones x) {

        Secuencia<Megacorporacion> ef = this.megacorporaciones.getUnion(x.megacorporaciones);

        ListadoMegacorporaciones corps = new ListadoMegacorporaciones();

        corps.setMegacorporaciones(ef);

        return corps;

    }

    public ListadoMegacorporaciones getInterseccion(ListadoMegacorporaciones x) {
        Secuencia<Megacorporacion> ef = this.megacorporaciones.getInterseccion(x.megacorporaciones);

        ListadoMegacorporaciones corps = new ListadoMegacorporaciones();

        corps.setMegacorporaciones(ef);

        return corps;
    }

    public ListadoMegacorporaciones getDiferencia(ListadoMegacorporaciones x) {
        Secuencia<Megacorporacion> ef = this.megacorporaciones.getDiferencia(x.megacorporaciones);

        ListadoMegacorporaciones corps = new ListadoMegacorporaciones();

        corps.setMegacorporaciones(ef);

        return corps;
    }

    public ListadoMegacorporaciones getDiferenciaSimetrica(ListadoMegacorporaciones x) {
        Secuencia<Megacorporacion> ef = this.megacorporaciones.getDiferenciaSimetrica(x.megacorporaciones);

        ListadoMegacorporaciones corps = new ListadoMegacorporaciones();

        corps.setMegacorporaciones(ef);

        return corps;
    }

    public ListadoMegacorporaciones getConjuntosPares(){
        ListadoMegacorporaciones unit = new ListadoMegacorporaciones();
        unit.setMegacorporaciones(megacorporaciones.getConjuntosPares());;
     
        return unit;
    }

    public Secuencia<Megacorporacion> getMegacorporaciones() {
        return this.megacorporaciones;
    }

    public void setMegacorporaciones(Secuencia<Megacorporacion> megacorporaciones) {
        this.megacorporaciones = megacorporaciones;
    }
}

